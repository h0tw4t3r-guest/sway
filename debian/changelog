sway (1.2-1) experimental; urgency=medium
  * New upstream version
  * Abdelhakim Qbaich (1):
   - Add the missing constant for the switch input type

  * Aidan Harris (1):
   - Fix segfault with "xwayland false" (#4228)

  * Alex Maese (1):
   - Unhide cursor on cursor activity after touch

  * Alyssa Ross (2):
   - bar: fix segfault with missing or invalid bar id
   - commands/bar: improve error for invalid subcommand

  * Antonin Décimo (7):
   - Allocator sizeof operand mismatch
   - Remove unused variable
   - ipc-client: remove useless free for failed malloc
   - input: check pointer against nullptr
   - view_update_size: fix surface_width/height mismatch
   - Fix memory leaks
   - Remove redundant checks

  * Ashkan Kiani (1):
   - Avoid adding duplicate criteria for no_focus and command

  * Brian Ashworth (52):
   - Destroy swaybg client on reload
   - config/output: fix typo in merge_id_on_name
   - Spawn swaybar as a wayland client
   - input/keyboard: attempt default keymap on failure
   - commands/bar: fix mode and hidden_state at runtime
   - input/switch: fix indentation of file
   - common/ipc-client: remove ipc recv timeout log
   - bindings: allow unlocked and locked bindings
   - Add swaybar protocol documentation
   - cmd_hide_edge_borders: add missing arg count check
   - criteria: reset on semicolon separation
   - commands/input: perform basic keymap validation
   - config/input: validate xkb keymap before storing
   - bindings: defer while initiailizing
   - config/xwayland: retain xwayland status on reload
   - config: fix find_handler logic
   - cmd_seat: split action and config handlers
   - cmd_mode: allow runtime creation and modification
   - ws-output-priority: fix logic issue in find_output
   - swaybar-protocol.7: fix block border descriptions
   - cmd_bindswitch: add option to execute on reload
   - input/libinput: only reset supported options
   - config/output: correctly set width/height in apply
   - cmd_layout: toggle split for tabbed/stack default
   - cmd_split: fix toggle split for non-split layouts
   - cmd_output: support current output alias
   - input_cmd_xkb_switch_layout: support input types
   - ipc: add an input event
   - ipc: add input::libinput_config event
   - arrange: remove gaps for workspace location deltas
   - input/libinput: fix typo in set_middle_emulation
   - libinput: fix set_send_events
   - input/keyboard: don't reset layout for same keymap
   - bindsym/code: add group support
   - input/libinput: typo fixes (get -> get_default)
   - cmd_swap: add floating support
   - input/cursor: do not hide when buttons are pressed
   - swaymsg: return 2 for sway errors
   - cmd_mode: make modes case sensitive
   - config/output: rebase cursors after config applied
   - sway.5: explain how to enable pango markup in font
   - handle_seat_node_destroy: do not focus own node
   - workspace: do not destroy if any seat is focusing
   - swaymsg.1: add tip about two hyphens for commands
   - container_replace: copy {width,height}_fraction
   - workspace_split: focus middle if workspace focused
   - sway{,-bar}.5: add link to pango font description
   - input/seatop_down: add axis handler
   - input/keyboard: send released only if pressed sent
   - sway.5: remove mention of floating_scroll
   - cmd_move: fix move to scratchpad hidden container
   - cmd_mode: don't reset to default after subcommand

  * Daniel Eklöf (3):
   - add seat sub command 'xcursor_theme'
   - swaybar/nag: use xcursor theme defined by XCURSOR_THEME/SIZE
   - check for empty string before calling strtoul() and check errno

  * Dark (1):
   - Update titlebar borders to match i3's current behavior.

  * Drew DeVault (9):
   - Add Firefox note to ISSUE_TEMPALTE.md
   - Create FUNDING.yml
   - Bump meson version to 1.1
   - Remove rootston from build manifests
   - layer-shell: add support for popups
   - Add _incr_version to contrib/
   - chmod +x contrib/_incr_version
   - Further refinements to _incr_version script
   - Update version to 1.2

  * Ed Younis (2):
   - Implement input_cmd_xkb_file (#3999)
   - input_cmd_xkb_*: cleanup includes

  * Ilia Bozhinov (1):
   - properly check pixman_region32_contains_rectangle return

  * Jeff Peeler (1):
   - cmd_opacity: add relative opacity changes

  * Josef Gajdusek (4):
   - Implement wlr-output-management-v1
   - Update output manager config on all output events
   - Provide current DPMS state in GET_OUTPUTS
   - Update output manager on layout change

  * Kenny Levinsen (1):
   - Use parent get_root_coords in subsurfaces

  * Laurent Bonnans (1):
   - ipc: collapse multi-container command results

  * Manuel Stoeckl (2):
   - Replace meson generator with custom_target
   - Use -fmacro-prefix-map to strip build path

  * Matt Coffin (1):
   - Fix segfaults caused by faulty command parsing

  * Michael Aquilina (1):
      Fix formatting for title_format in man 5 sway Use explicit linebreaks to make scdoc use a separate line for each entry listed

  * Moelf (1):
   - Implement output toggle

  * Nick Paladino (1):
   - Make comment casing consistient

  * Nomeji (1):
   - Add infos to help using for_window to man 5

  * Paul Ouellette (1):
   - Fix typo in sway(5) manpage

  * Pedro Côrte-Real (7):
   - Layout correctly with several new windows
   - Layout tiled using a width/height fraction
   - Rework gaps code to be simpler and correct
   - Sanity check gaps between tiled containers
   - Avoid negative outer gaps
   - Sanity check gaps on the outside of the workspace
   - Fix resize sibling amount calculations

  * Robert Sacks (1):
   - Add missing underscore in bindswitch documentation

  * Rouven Czerwinski (3):
   - desktop: output: fix use-after-free in destroy
   - Fix sway crashes for scratchpad layouts
   - Revert "Add support for wlr_output's atomic API"

  * Sauyon Lee (1):
   - Make fullscreen check for fullscreen parents

  * Sebastian Krzyszkowiak (1):
   - layer_shell: Guard against negative exclusive zone

  * Sebastian Parborg (1):
   - Make mouse drag in tiled mode swap containers if no edge is selected

  * Sergei Dolgov (4):
   - Add calibration_matrix config option
   - Use isnan
   - calibration_matrix: add the current matrix to the IPC description for libinput devices
   - calibration_matrix: expect 6 individual values

  * Simon Ser (8):
   - Fix xdg-decoration unconfigured if set before first commit
   - Remove orbital gamma-control protocol
   - ipc: add xkb_layout_names and xkb_active_layout_index
   - Add a new xkb_switch_layout command
   - Add missing docs for xkb_switch_layout
   - Add docs for new IPC keyboard properties
   - Remove all wayland-server.h includes
   - Add support for wlr_output's atomic API

  * asdfjkluiop (1):
   - A layer-shell will only be focused if it is non-null

  * jasperro (1):
   - Added Dutch translation of README

  * lbonn (1):
   - Allow moving a container hidden in scratchpad

  * murray (1):
   - use surface coordinates for damaging buffers

  * random human (1):
   - tree: set correct border value before creating floater

  * rpigott (1):
   - Add missing description for focus_on_window_activation command in docs.

 -- Vladyslav Dalechyn <h0tw4t3r@parrotsec.org>  Wed, 11 Sep 2019 10:20:30 +0200

sway (1.1.1-1) experimental; urgency=medium

  * New upstream version (Closes: #927723)
  * d/control:
   - Add swaybg to dependencies as proposed by upstream
     ("swaybg is a required dependency of sway")
   - Add suckless-tools and x-terminal-emulator to Recommends,
     because the default sway config uses dmenu(1) (Closes: #924063)
   - Remove workaround for libwlroots run dependency from Depends:
     field. Since libwlroots1 we can rely on dpkg-shlibdeps
   - Make build-deps libsystemd-dev and libelogind-dev alternatives
   - Set meson version for build-dep like upstream
     does in meson.build (Closes: #927915)
   - Add libgl1-mesa-dri as dependency (Closes: #927990)
   - Set strict version of libwlroots dependency: the library breaks
     ABI between versions so we need the right version for building sway
   - Run wrap-and-sort on d/control
   - Add missing build-dependencies: libevdev-dev, libpixman-1-dev,
     libwayland-egl1 and libxbcommon-dev

  * d/patches
   - Add patch to install zsh completions into correct directory
     (Closes: #922777)
   - Add patch to replace urxvt in the default config file with
     x-terminal-emulator
   - Add patch to use the correct version string

  * d/copyright
   - Add missing files
   - Bump copyright years on debian/

  * d/sway.install
   - Add directory for manpages in section 7
   - Update path of zsh completions directory

 -- Birger Schacht <birger@rantanplan.org>  Sun, 09 Jun 2019 07:38:17 +0200

sway (1.0~rc3-1) experimental; urgency=medium

  * New upstream version

 -- Birger Schacht <birger@rantanplan.org>  Tue, 19 Feb 2019 13:28:38 +0100

sway (1.0~rc2-1) experimental; urgency=medium

  * New upstream version
  * d/rules: remove sway_version argument, as the version
    is now set in the build file

 -- Birger Schacht <birger@rantanplan.org>  Tue, 12 Feb 2019 19:49:19 +0100

sway (1.0~rc1-1) experimental; urgency=medium

  * New upstream release
  * Remove swaylock and swayidle: they are now separate packages
  * Tidied up d/sway.install: now that swaylock and swayidle
    are separate source packages, its easier to use wildcards
  * Specify versioned build-dependencies as required by upstream.
    Thus explicitly listing wayland-protocols and libsystemd as
    build-dep with versions and adding versions to scdoc build-dep.
  * Adding libelogind build-dep
  * Bump version of libwlroots-dev build-dep
  * Remove references to swaygrab (it was removed from the upstream
    package; see grim(1) for an alternative)
  * Update Recommends and Suggests: one of the sway wallpapers is
    configured as background in the shipped config file, thus the
    package containing the wallpapers is recommended. swayidle and
    swaylock are also configured in the shipped config file, but
    commented, thus they are only suggested
  * Make versioned run dependency on libwlroots0 explicit

 -- Birger Schacht <birger@rantanplan.org>  Sun, 10 Feb 2019 09:48:12 +0100

sway (1.0~beta.2-3) experimental; urgency=medium

  * Try upload again.

 -- Birger Schacht <birger@rantanplan.org>  Thu, 31 Jan 2019 12:57:01 +0100

sway (1.0~beta.2-2) experimental; urgency=medium

  * Fixed license identifier in d/copyright

 -- Birger Schacht <birger@rantanplan.org>  Wed, 30 Jan 2019 23:26:43 +0100

sway (1.0~beta.2-1) experimental; urgency=medium

  [ Nicolas Braud-Santoni & Birger Schacht ]
  * Initial packaging (Closes: 897246, 821397)

 -- Birger Schacht <birger@rantanplan.org>  Wed, 30 Jan 2019 21:11:27 +0100
